Here described the future releases for the project, details in [CHANGELOG](CHANGELOG.md)

| Version | Release Date   | Main Features                                                          |
|---------|----------------|------------------------------------------------------------------------|
| 0.1.0   | February 2024  | - Initial project setup and configuration                              |
|         |                | - Basic CRUD operations for wallets, categories, labels, and expenses  |
|         |                | - API endpoints for managing wallets, categories, labels, and expenses |
|         |                | - Database integration with SQLite and PostgreSQL                      |
|         |                | - Basic unit testing                                                   |
| 0.2.0   | June 2024      | - Added budgeting features                                             |
|         |                | - Enhanced CRUD operations for budget management                       |
|         |                | - New API endpoints for budget tracking and  analysis                  |
| 0.3.0   | November 2024  | - Added data visualization with charts                                 |
|         |                | - New endpoints for chart data retrieval                               |
|         |                | - Enhanced analytics for expenses and budgets                          |
