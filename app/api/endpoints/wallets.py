from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...crud import crud_wallet
from ...db.session import get_db
from ...schemas.wallet import WalletCreate, Wallet

router = APIRouter()

@router.get("/wallets/", response_model=List[Wallet])
def read_wallets(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud_wallet.get_wallets(db, skip=skip, limit=limit)

@router.post("/wallets/", response_model=Wallet)
def create_wallet(wallet: WalletCreate, db: Session = Depends(get_db)):
    return crud_wallet.create_wallet(db=db, wallet=wallet)

@router.get("/wallets/{wallet_id}", response_model=Wallet)
def read_wallet(wallet_id: int, db: Session = Depends(get_db)):
    db_wallet = crud_wallet.get_wallet(db, wallet_id=wallet_id)
    if db_wallet is None:
        raise HTTPException(status_code=404, detail="Wallet not found")
    return db_wallet
