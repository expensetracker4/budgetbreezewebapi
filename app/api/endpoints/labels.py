from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from typing import List

from ...crud import crud_label
from ...db.session import get_db
from ...schemas.label import LabelCreate, Label

router = APIRouter()

@router.get("/labels/", response_model=List[Label])
def read_labels(db: Session = Depends(get_db)):
    return crud_label.get_labels(db)

@router.post("/labels/", response_model=Label)
def create_label(label: LabelCreate, db: Session = Depends(get_db)):
    return crud_label.create_label(db=db, label=label)
