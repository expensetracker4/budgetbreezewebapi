from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...crud import crud_expense
from ...db.session import get_db
from ...schemas.expense import ExpenseCreate, Expense

router = APIRouter()

@router.get("/expenses/", response_model=List[Expense])
def read_expenses(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud_expense.get_expenses(db, skip=skip, limit=limit)

@router.post("/expenses/", response_model=Expense)
def create_expense(expense: ExpenseCreate, db: Session = Depends(get_db)):
    return crud_expense.create_expense(db=db, expense=expense)
