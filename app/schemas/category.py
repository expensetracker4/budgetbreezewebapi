from pydantic import BaseModel
from typing import Optional

class CategoryBase(BaseModel):
    id: str
    name: str
    description: Optional[str] = None

class CategoryCreate(CategoryBase):
    pass

class CategoryUpdate(CategoryBase):
    pass

class Category(CategoryBase):
    pass
    class Config:
        orm_mode = True
