from pydantic import BaseModel

class ExpenseLabelBase(BaseModel):
    expense_id: str
    label_id: str

class ExpenseLabelCreate(ExpenseLabelBase):
    pass

class ExpenseLabel(ExpenseLabelBase):
    pass
    class Config:
        orm_mode = True
