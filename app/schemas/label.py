from pydantic import BaseModel

class LabelBase(BaseModel):
    id: str
    name: str

class LabelCreate(LabelBase):
    pass

class LabelUpdate(LabelBase):
    pass

class Label(LabelBase):
    pass
    class Config:
        orm_mode = True
