from pydantic import BaseModel

class WalletBase(BaseModel):
    id: str
    name: str
    balance: float
    currency: str

class WalletCreate(WalletBase):
    pass

class WalletUpdate(WalletBase):
    pass

class Wallet(WalletBase):
    pass
    class Config:
        orm_mode = True
