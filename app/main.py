from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

# Import routers from different modules
from .api.endpoints import wallets, categories, labels, expenses

app = FastAPI(title="Expense Tracker API", version="1.0.0")

# CORS middleware configuration
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allows all origins
    allow_credentials=True,
    allow_methods=["*"],  # Allows all methods
    allow_headers=["*"],  # Allows all headers
)

# Include routers from different modules
app.include_router(wallets.router, prefix="/wallets", tags=["wallets"])
app.include_router(categories.router, prefix="/categories", tags=["categories"])
app.include_router(labels.router, prefix="/labels", tags=["labels"])
app.include_router(expenses.router, prefix="/expenses", tags=["expenses"])

@app.get("/")
async def root():
    return {"message": "Welcome to the Expense Tracker API"}

# Add more endpoints as needed
