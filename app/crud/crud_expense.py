from sqlalchemy.orm import Session
from ..db.models.expense import Expense
from ..schemas.expense import ExpenseCreate, ExpenseUpdate

def get_expense(db: Session, expense_id: str):
    return db.query(Expense).filter(Expense.id == expense_id).first()

def get_expenses(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Expense).offset(skip).limit(limit).all()

def create_expense(db: Session, expense: ExpenseCreate):
    db_expense = Expense(**expense.model_dump())
    db.add(db_expense)
    db.commit()
    db.refresh(db_expense)
    return db_expense

def update_expense(db: Session, expense_id: str, expense: ExpenseUpdate):
    db.query(Expense).filter(Expense.id == expense_id).update(expense.model_dump())
    db.commit()
    return get_expense(db, expense_id)

def delete_expense(db: Session, expense_id: str):
    db_expense = get_expense(db, expense_id)
    if db_expense:
        db.delete(db_expense)
        db.commit()
