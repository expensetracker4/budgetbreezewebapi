from sqlalchemy.orm import Session
from ..db.models.label import Label
from ..schemas.label import LabelCreate, LabelUpdate

def get_label(db: Session, label_id: str):
    return db.query(Label).filter(Label.id == label_id).first()

def get_labels(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Label).offset(skip).limit(limit).all()

def create_label(db: Session, label: LabelCreate):
    db_label = Label(**label.model_dump())
    db.add(db_label)
    db.commit()
    db.refresh(db_label)
    return db_label

def update_label(db: Session, label_id: str, label: LabelUpdate):
    db.query(Label).filter(Label.id == label_id).update(label.model_dump())
    db.commit()
    return get_label(db, label_id)

def delete_label(db: Session, label_id: str):
    db_label = get_label(db, label_id)
    if db_label:
        db.delete(db_label)
        db.commit()
