from sqlalchemy.orm import Session
from ..db.models.category import Category
from ..schemas.category import CategoryCreate, CategoryUpdate

def get_category(db: Session, category_id: str):
    return db.query(Category).filter(Category.id == category_id).first()

def get_categories(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Category).offset(skip).limit(limit).all()

def create_category(db: Session, category: CategoryCreate):
    db_category = Category(**category.model_dump())
    db.add(db_category)
    db.commit()
    db.refresh(db_category)
    return db_category

def update_category(db: Session, category_id: str, category: CategoryUpdate):
    db.query(Category).filter(Category.id == category_id).update(category.model_dump())
    db.commit()
    return get_category(db, category_id)

def delete_category(db: Session, category_id: str):
    db_category = get_category(db, category_id)
    if db_category:
        db.delete(db_category)
        db.commit()
