from sqlalchemy.orm import Session
from ..db.models.wallet import Wallet
from ..schemas.wallet import WalletCreate, WalletUpdate

def get_wallet(db: Session, wallet_id: str):
    return db.query(Wallet).filter(Wallet.id == wallet_id).first()

def get_wallets(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Wallet).offset(skip).limit(limit).all()

def create_wallet(db: Session, wallet: WalletCreate):
    db_wallet = Wallet(**wallet.model_dump())
    db.add(db_wallet)
    db.commit()
    db.refresh(db_wallet)
    return db_wallet

def update_wallet(db: Session, wallet_id: str, wallet: WalletUpdate):
    db.query(Wallet).filter(Wallet.id == wallet_id).update(wallet.model_dump())
    db.commit()
    return get_wallet(db, wallet_id)

def delete_wallet(db: Session, wallet_id: str):
    db_wallet = get_wallet(db, wallet_id)
    if db_wallet:
        db.delete(db_wallet)
        db.commit()
