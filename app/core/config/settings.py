import os
from pydantic_settings import BaseSettings

class Settings(BaseSettings):
    # General settings
    APP_NAME: str = os.getenv("APP_NAME", "Expense Tracker API")
    APP_VERSION: str = "1.0.0"
    API_PREFIX: str = os.getenv("API_PREFIX", "/api")
    DB_TYPE: str = str.lower(os.getenv("DB_TYPE", "sqlite"))
    SQLALCHEMY_DATABASE_URI: str = ""

    # Add more settings as needed, e.g., security, logging, etc.

    # Database settings
    if  DB_TYPE == "sqlite" or DB_TYPE == "localdb":
        SQLITE_DBNAME: str = os.getenv("SQLITE_DBNAME", "budgetbreeze.db")
        SQLALCHEMY_DATABASE_URI: str = "sqlite:///./{dbname}".format(dbname=SQLITE_DBNAME)
    elif DB_TYPE == "pgsql" or DB_TYPE == "postgresql":
        PGSQL_USERNAME: str = os.getenv("PGSQL_USERNAME", "budgetbreeze")
        PGSQL_PASSWORD: str = os.getenv("PGSQL_PASSWORD", "budgetbreeze")
        PGSQL_HOST: str = os.getenv("PGSQL_HOST", "localhost")
        PGSQL_PORT: str = os.getenv("PGSQL_PORT", "5432")
        PGSQL_DBNAME: str = os.getenv("PGSQL_DBNAME", "budgetbreeze")
        SQLALCHEMY_DATABASE_URI: str = "postgresql://{user}:{password}@{host}:{port}/{dbname}".format(
            user=PGSQL_USERNAME,
            password=PGSQL_PASSWORD,
            host=PGSQL_HOST,
            port=PGSQL_PORT,
            dbname=PGSQL_DBNAME)
    elif DB_TYPE == "mysql" or DB_TYPE == "mariadb":
        MYSQL_USERNAME: str = os.getenv("MYSQL_USERNAME", "budgetbreeze")
        MYSQL_PASSWORD: str = os.getenv("MYSQL_PASSWORD", "budgetbreeze")
        MYSQL_HOST: str = os.getenv("MYSQL_HOST", "localhost")
        MYSQL_PORT: str = os.getenv("MYSQL_PORT", "3306")
        MYSQL_DBNAME: str = os.getenv("MYSQL_DBNAME", "budgetbreeze")
        SQLALCHEMY_DATABASE_URI: str = "mysql+pymysql://{user}:{password}@{host}:{port}/{dbname}".format(
            user=MYSQL_USERNAME,
            password=MYSQL_PASSWORD,
            host=MYSQL_HOST,
            port=MYSQL_PORT,
            dbname=MYSQL_DBNAME)
    else:
        raise Exception("Database " + DB_TYPE + " not supported, change DB_TYPE variable")

    # Security measure: empty all sensitive envs
    os.environ["SQLITE_DBNAME"] = ""

    os.environ["PGSQL_USERNAME"] = ""
    os.environ["PGSQL_PASSWORD"] = ""
    os.environ["PGSQL_HOST"] = ""
    os.environ["PGSQL_PORT"] = ""
    os.environ["PGSQL_DBNAME"] = ""

    os.environ["MYSQL_USERNAME"] = ""
    os.environ["MYSQL_PASSWORD"] = ""
    os.environ["MYSQL_HOST"] = ""
    os.environ["MYSQL_PORT"] = ""
    os.environ["MYSQL_DBNAME"] = ""


    class Config:
        # Loads the environment variables when the application starts
        env_file = ".env"


settings = Settings()
