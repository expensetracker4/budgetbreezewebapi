# db/session.py
from typing import Generator
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from ..core.config.settings import settings

# Database URL (change as needed for your environment)
DATABASE_URL = settings.SQLALCHEMY_DATABASE_URI

# Create the SQLAlchemy engine
engine = create_engine(DATABASE_URL)

# SessionLocal class will be a factory for new Session objects
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Base class for your models
Base = declarative_base()

def get_db() -> Generator:
    """
    Dependency to get a database session.
    Use it in endpoints to access the database.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()