# Expense Tracker Database Schema

## Tables

### 1. Wallets

- **id**: UUID (Primary Key)
- **name**: String
- **balance**: Float
- **currency**: String

### 2. Categories

- **id**: UUID (Primary Key)
- **name**: String
- **description**: String (Optional)

### 3. Labels

- **id**: UUID (Primary Key)
- **name**: String

### 4. Expenses

- **id**: UUID (Primary Key)
- **amount**: Float
- **date**: DateTime
- **wallet_id**: UUID (Foreign Key to Wallets)
- **category_id**: UUID (Foreign Key to Categories, Optional)
- **notes**: String (Optional)

### 5. ExpenseLabels (Many-to-Many Relationship)

- **expense_id**: UUID (Foreign Key to Expenses)
- **label_id**: UUID (Foreign Key to Labels)

## Relationships

- Each **Expense** is associated with one **Wallet**.
- Each **Expense** can be associated with one **Category**.
- Each **Expense** can have multiple **Labels** (through ExpenseLabels).

## Notes

- **UUID** is used for the primary key for better scalability and to avoid conflicts.
- **Foreign Key** constraints are used to maintain referential integrity.
- **ExpenseLabels** is a join table facilitating a many-to-many relationship between Expenses and Labels.
