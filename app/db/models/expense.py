from sqlalchemy import Column, Float, DateTime, ForeignKey, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from ..base import Base

class Expense(Base):
    __tablename__ = "expenses"

    id = Column(String, primary_key=True, server_default=func.uuid_generate_v4())
    amount = Column(Float, nullable=False)
    date = Column(DateTime, server_default=func.now())
    wallet_id = Column(String, ForeignKey("wallets.id"), nullable=False)
    category_id = Column(String, ForeignKey("categories.id"))
    notes = Column(String)

    wallet = relationship("Wallet")
    category = relationship("Category")
