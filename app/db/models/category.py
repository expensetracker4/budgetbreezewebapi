from sqlalchemy import Column, String
import uuid

from ..base import Base

class Category(Base):
    __tablename__ = "categories"

    id = Column(String, primary_key=True, default=uuid.uuid4())
    name = Column(String, nullable=False)
    description = Column(String)
