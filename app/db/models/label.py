from sqlalchemy import Column, String
import uuid

from ..base import Base

class Label(Base):
    __tablename__ = "labels"

    id = Column(String, primary_key=True, default=uuid.uuid4())
    name = Column(String, nullable=False)
