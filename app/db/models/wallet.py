from sqlalchemy import Column, String, Float
import uuid

from ..base import Base

class Wallet(Base):
    __tablename__ = "wallets"

    id = Column(String, primary_key=True, default=uuid.uuid4())
    name = Column(String, nullable=False)
    balance = Column(Float, nullable=False)
    currency = Column(String, nullable=False)
