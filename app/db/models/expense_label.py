from sqlalchemy import Column, ForeignKey, String
from sqlalchemy.orm import relationship

from ..base import Base

class ExpenseLabel(Base):
    __tablename__ = "expense_labels"

    expense_id = Column(String, ForeignKey("expenses.id"), primary_key=True)
    label_id = Column(String, ForeignKey("labels.id"), primary_key=True)

    expense = relationship("Expense", back_populates="labels")
    label = relationship("Label", back_populates="expenses")
