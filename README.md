# BudgetBreezeWebapi

BudgetBreezeWebapi is a modern, fast, and efficient backend API designed for managing personal finances and tracking expenses. Built with FastAPI and SQLAlchemy, it offers robust features including wallets management, expense tracking, multi-currency support, and much more.

## Features

- **Wallets Management**: Manage multiple wallets and view their balances.
- **Categories Management**: Categorize expenses for better organization and tracking.
- **Labels Management**: Add labels to expenses for enhanced searchability.
- **Expense/Income Tracking**: Record and track all your expenses and incomes.
- **Multi-Currency Support**: Handle multiple currencies and convert them on the fly.
- **Open Banking API Integration** (Optional): Connect to banking services for real-time financial data.

## Technology Stack

- **Backend**: Python, FastAPI
- **Database**: PostgreSQL (Production), SQLite (Development)
- **ORM**: SQLAlchemy

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them:

- Python 3.8+
- pip

### Installing

A step-by-step series of examples that tell you how to get a development environment running.

Clone the repository:

```bash
git clone https://gitlab.com/expensetracker4/budgetbreezewebapi.git
cd BudgetBreezeWebapi
```

Install the required packages:

```bash
pip install -r requirements.txt
```

Set up the database (make sure you have PostgreSQL or SQLite installed):

```bash
# For PostgreSQL
export DB_TYPE=postgresql

# For SQLite
export DB_TYPE=sqlite
```

Run the migrations:

```bash
alembic upgrade head
```

Start the server:

```bash
uvicorn app.main:app --reload
```

## Running the Tests

Explain how to run the automated tests for this system:

```bash
pytest
```

## API Documentation

Once the server is running, you can view the API documentation at `http://localhost:8000/docs`.

## Deployment

Add additional notes about how to deploy this on a live system using Docker.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/expensetracker4/budgetbreezewebapi/tags).

## Authors

- **Daniele Viti** - *Initial work* - [Daniele Viti](https://gitlab.com/dnviti)

See also the list of [contributors](https://gitlab.com/expensetracker4/budgetbreezewebapi/project_members) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

- Hat tip to anyone whose code was used
- Inspiration
- etc
