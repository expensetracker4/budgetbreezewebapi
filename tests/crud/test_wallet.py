from sqlalchemy.orm import Session
from app.crud.crud_wallet import create_wallet, get_wallets
from app.schemas.wallet import WalletCreate

def test_create_wallet(db: Session):
    wallet_data = {"name": "Test Wallet", "balance": 100.0}
    wallet = create_wallet(db, WalletCreate(**wallet_data))
    assert wallet.name == wallet_data["name"]
    assert wallet.balance == wallet_data["balance"]

def test_get_wallets(db: Session):
    wallet_list = get_wallets(db)
    assert len(wallet_list) >= 0
