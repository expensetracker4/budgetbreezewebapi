# Changelog

All notable changes to the BudgetBreezeWebapi project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Initial project setup and configuration.
- Database models for wallets, categories, labels, and expenses.
- CRUD operations for each database model.
- Basic API endpoints for wallet, category, label, and expense management.
- Integration with Alembic for database migrations.
- Configuration for switching between PostgreSQL and SQLite using an environment variable.
- Pydantic models for request and response validation.
- Basic unit tests for CRUD operations.

### Changed
- N/A

### Deprecated
- N/A

### Removed
- N/A

### Fixed
- N/A

### Security
- N/A

<!-- Template for new entries
## [0.1.0] - 2023-12-12

### Added
- Initial release of BudgetBreezeWebapi.
- README.md with project setup and usage instructions.

### Changed
- N/A

### Deprecated
- N/A

### Removed
- N/A

### Fixed
- N/A

### Security
- N/A

## [0.1.1] - 2024-01-10

### Added
- Added support for multi-currency transactions.
- Implemented external API integration for currency conversion.

### Changed
- Improved error handling in API endpoints.

### Deprecated
- N/A

### Removed
- N/A

### Fixed
- Fixed a bug in currency conversion calculations.

### Security
- N/A


## [Version] - YYYY-MM-DD

### Added
- For new features.

### Changed
- For changes in existing functionality.

### Deprecated
- For soon-to-be removed features.

### Removed
- For now removed features.

### Fixed
- For any bug fixes.

### Security
- In case of vulnerabilities.
-->

